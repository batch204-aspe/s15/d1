console.log("Hello World"); //logs a message on the console
/*
	Syntax:
			console.log(message);
	STatement:
			console.log("Hello World");
*/


// Writing Comments
	// COmments are meant to describe the written code.
// Two ways of writing conments in JS
	/*
		1. Single Line
			► // ctrl + /

		2. Multi Line Comment
			► (ctrl + shift + / )
	*/

/*
	Variables 
			► it used to contain date.
			► when we create variables, certain portion
			  of a device's memory is given a
			  "NAME" taht we call "variables"

		Declaring Variable
			Syntax:
					let/const variableName;
				let is a keyword that is usually used in declaring a variable

*/

let myVariable = 500;
console.log(myVariable);

//Variable must be declared first before they are used
// console.log(hello); // hello is not defined

/*

	Best Practice in Naming Variables
		1.	When naming variables, it is important to create
			variables that are descriptive and indicative of
		  	the data it contains.

		2. 	When naming variables, it is better to start w/
			a lower case.

		3.	Do not add spaces to the variable names. Use
			camelCase for multiple words.


*/


/*
	
	let firstName = "Mark Joseph"; //good variable name
	let pokemon = 25000; //bad variable name
	let FamilyName = "Mendoza"; //bad variable name;
	let familyName = "Mendoza"; // good variable name;

*/

/*
	
	Declaring & Initializing Variables
		Initializing Variables - the instance when a variable
								 is given its initial/starting value.
*/ 

let productName = 'desktop computer';
console.log(productName);

let productPrice = 59000;
console.log(productPrice);

const pi = 3.1416;
console.log(pi);

/*
	
	let & const are the keywords that we used to declare a variable
	
	let 
		► we usually use "let" if we want to reassign the values in our values

*/ 

//Reassigning a variable means changing it's initial or previous value into another value

/*
	Syntax:
		variableName = newValue;
*/

productName = "Laptop";
console.log(productName);

let bootcamper = "lexus";
bootcamper = "jayson";
console.log(bootcamper);
/*
	let student = "Mk";
	let student = "Jo"; //Identifier 'student' has already been declared
	console.log(student);

*/

/*
	const cannot be updated or redeclared
	values of constant cannot be changed and will simply return an error
	
	i.e.
	
	pi is already declared as conts pi = 3.1416;

	pi = 555;
    console.log(pi);

*/

/*
	When to use JS const?
		► As a general rule, always declare a variable w/
		  const unless you know that the value will change.

	Use const when you declare:
		☼ A new Array
		☼ A new Object
		☼ A new Function

*/

/*
	Reassigning variable vs Initializing variables
*/
// Declares a variable first
let supplier;
// Initialization is done after the variable has been declared;
console.log(supplier);
// This is considered as initialization because it is the
// 1st time that a value has been assigned to a variable;
supplier = "John Smith Tradings";
// let supplier = "John Smith Tradings"; This is a shortcut for declaring a value
console.log(supplier);
// This is considered as reassignment because its initial
// value was already declared
supplier = "Zuitt Store";
console.log(supplier);
/*
	Can you declare a const variable without initialization?
	No Ann Error will Occur

	// const interestRate;
	// interestRate = 5.15;
	// console.log(interestRate);

	const variables are variables w/ constant data. Therefore, we should
	not re-declare, re-assign, or even a const variable without
	initialization.
*/


/*************************************/
/*
	
	var vs let/const

	var - is also used in declaring a variable,
		  but var is an ECMAScript1(ES1) feature

	let/const - introduced as a new feature in ES6 updates

*/

// What makes let/const different from var
// example
	a=5;
	console.log(a);
	var a;

	// b=5;
	// console.log(b);
	// let b;

//let/const scope
// Scope essentially means where these variables are available for use
// let & const are block scope
// A block is a chunck of code bounded br {}.
// A block lives in curly braces. Anything within curly braces is a block.
	
	let outerVariable = "Hello"; //global scope
	{
		let innerVariable = "Hello Again"; //locally scope
		console.log(innerVariable);
	}
	console.log(outerVariable);


// var - global scope
var outerVariable1 = "Hello - Var";
{
	var innerVariable1 = "Hello Again - Var";
}
console.log(outerVariable1);
console.log(innerVariable1);

//  Multiple Variable declarations
let productCode = 'DC017', productBrand = "Dell"; // you can use this to declare multiple varialble
// however it is best use to declare it separately for readbility purpose
//let productCode = 'DC017'; 
//let productBrand = "Dell";
console.log(productCode, productBrand);
/*
	
	We cannot use a reserved keyword as a variable name
	
	example
		const let = "Hello";
		console.log(let);

*/

/* DATA TYPES
	
	1. Strings
			► a series of characters that create a word, a phrase, a sentence
			or anything related to creating a text
			► can be written either a single ( ' ) or using double qoute( " )
	
	2. 
*/ 
// String Data Types
let country = 'Philippines';
let province = "Metro Manila";
console.log(country);
console.log(province);

// Concatenate Strings
let fullAdress = province + ', ' + country;
console.log(fullAdress);

let greeting = 'I live in the ' + country;
console.log(greeting);

/*
	The escape character ( \ ) in strings in combination w/ other characters
	that can produce different effects.

	☼ "\n" - refers to creating a NEW LINE in between text

*/

let mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress);

let message = "John's employyes went home early.";
console.log(message);

message = 'John\'s employyes went home early.';
console.log(message);

message = 'John\'s employyes went home "early."';
console.log(message);

//using BackTicks ( ` ` )
message = `John's employyes went home "early."`;
console.log(message);

// NUMBER DATA TYPE
// Declaring Whole Number ► it can be a positive & negative numbers
let headCount = 20;
console.log(headCount);
let numberNegative = -8;
console.log(numberNegative);
// Declaring  Decimal Number
let grade = 98.7;
console.log(grade);
// Declaring  Exponential Number
let planetDistance = 2e10;
console.log(planetDistance);

// Combining Number & Strings Data Types
console.log("John's grade last quarter is " + grade);

/*
	Boolean Values are normally used to store values relating to the
	state of certain things

	this will be useful in further discussions about creating logic to make
	our application respond to certain scenarios
*/

let isMarried = false;
let isSingle = true;

console.log("is 25 yrs. old is the marrying age in the Philippines " + isMarried);
console.log("Is MK is Married? " + isMarried);
console.log("Is MK is SIngle? " + isSingle);


/*
	Arrays
		► 	are a special kind of data type that's used to store
			multiple values, but it is normally used to store similar
			data type.
	SYntax:
			let/const arrayName = [element0, element1, ...];
*/
let basketOfFruits = ["Bananas", "Apple", "Mango", "Strawberry"];
console.log(basketOfFruits);

// similar datatypes
let grades = [98.7, 85, 90, 93, 95.3];
console.log(grades);

//different datatypes
//However Storing different data type inside an array is nor recommended
//because it will not make sense in the contact of programming
let random = ["Jungkook", 24, true];
console.log(random);

/*
	OBJECTS
		► 	are another special kind data type that's used to mimic
			real world objects/items
		► 	They're used to create complex data that contains pieces
			of information that are relevant to each other.
		►	Every individual piece of information is called property of object.
		
		► They're also useful for creating abstract objects

	SYNTAX:
			let/const objectName = {
				
				propertyA: valueA,
				propertyB: valueB,
				propertyC: valueC,
				.		 :	.
				.		 :	.
				.		 :	.
			};
*/

let person = {
	fullname:"MK Aspe",
	age: 35,
	isMarried: false,
	contactNumber: ["09772989441", "09767767762"],
	address: {
		houseNumber: 345,
		village: "Future Homes",
		city: "Manila"
	}
};
console.log(person);

//Abstract Objects
const myGrades = {
	firstGrading: 98.7,
	secondGrading: 95,
	thirdGrading: 93,
	fourthGrading: 90.3
};
console.log(myGrades);

/*
	Typeof Operator
	 ► used to determine the type of data or the value of a variable
*/
console.log(typeof myGrades);
console.log(typeof grades); // result: Object (special type of object)
console.log(typeof basketOfFruits);
// note: Array is a special type of objects with methods & function to manipulate it
// for example
const anime = ["One Piece", "One Punch Man", "Attack on Titan"];
anime[0] = "Kimetsu no yaiba";
console.log(anime);
// We can change the element of an array assigned to a constant variable
// We can change the objects properties assigned to a constant variable

/*
	
	let anime = ["One Piece", "One Punch Man", "Attack on Titan"];
	anime = ['Kimetsu no yaiba'];
	console.log(anime);

*/

/*
	Constant Object & Array
		the keyword const is a litle misleading

		it does not define a constant value, it defines a constant reference to a value
		
		because of this you cannot:
			Reassign a constant value
					   constant array
					   constant object
		But you can:
			change the elements of constant array
			change the properties of constant object
*/

/*
	Null
		► it is uded to intentionally expess the absence of a value in a variable
		declaration/initialization.

*/
let spouse = null;
console.log(spouse);
// using null compared to a 0 value and an empty string is much better for readability purposes
// null is also considered as a data type of it's own compted to 0 which is a data type
// of a number and single quotes which are a data type of string

let myNumber = 0;
let myString = ' ';

/*
	Undefined
		► represents that state of a variable that has been declared but
		without any assigned value.
*/

let sssNumber;
console.log(sssNumber);
















